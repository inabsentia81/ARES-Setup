#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="lr-flycast"
rp_module_desc="Dreamcast emulator - Reicast port for libretro"
rp_module_help="Previously named lr-reicast then lr-beetle-dc\n\nDreamcast ROM Extensions: .cdi .gdi .chd, Naomi/Atomiswave ROM Extension: .zip\n\nCopy your Dreamcast/Naomi roms to $romdir/dreamcast\n\nCopy the required Dreamcast BIOS files dc_boot.bin and dc_flash.bin to $biosdir/dc\n\nCopy the required Naomi/Atomiswave BIOS files naomi.zip and awbios.zip to $biosdir/dc"
rp_module_licence="GPL2 https://raw.githubusercontent.com/libretro/flycast/master/LICENSE"
rp_module_section="lr"
rp_module_flags="!armv6"

function depends_lr-flycast() {
    local depends=()
    isPlatform "videocore" && depends+=(libraspberrypi-dev)
    isPlatform "mesa" && depends+=(libgles2-mesa-dev)
    getDepends "${depends[@]}"
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function _update_hook_lr-flycast() {
    renameModule "lr-reicast" "lr-beetle-dc"
    renameModule "lr-beetle-dc" "lr-flycast"
}

function sources_lr-flycast() {
    gitPullOrClone "$md_build" https://github.com/libretro/flycast.git
    # don't override our C/CXXFLAGS and set LDFLAGS to CFLAGS to avoid warnings on linking
    applyPatch "$md_data/01_flags_fix.diff"
}

function build_lr-flycast() {
    local params=("HAVE_LTCG=0")
    make clean
    if isPlatform "rpi"; then
        if isPlatform "rpi4"; then
            params+=("platform=rpi4")
        elif isPlatform "mesa"; then
            params+=("platform=rpi-mesa")
        else
            params+=("platform=rpi")
        fi
	    fi
	     
    if isPlatform "rockpro64"; then
        params+=("platform=RK3399 ARCH=arm")
    elif isPlatform "odroid-n2"; then
        params+=("platform=odroid-n2 ARCH=arm")
    elif isPlatform "jetson-nano"; then
        make platform=jetson-nano
    elif isPlatform "odroid-xu"; then
        params+=("platform=odroid BOARD="ODROID-XU4" ARCH=arm")
    elif isPlatform "tinker"; then
        params+=("platform=odroid BOARD="ODROID-XU4" ARCH=arm")
    fi
 # temporarily disable distcc due to segfaults with cross compiler and lto
    if isPlatform "odroid-n2"; then
	    make platform=odroid-n2 ARCH=arm
	elif ! isPlatform "jetson-nano"; then  #Disable distcc and lto break linker nvidia jetson nano needed  
        DISTCC_HOSTS="" make "${params[@]}"
	fi
	md_ret_require="$md_build/flycast_libretro.so"
}


function install_lr-flycast() {
    md_ret_files=(
        'flycast_libretro.so'
        'LICENSE'
    )
}

function configure_lr-flycast() {    
    # bios
									   

    mkUserDir "$biosdir/dc"
    
    local system
    for system in atomiswave dreamcast naomi; do
        mkRomDir "$system"
        ensureSystemretroconfig "$system"
        iniConfig " = " "" "$configdir/$system/retroarch.cfg"
        if isPlatform "odroid-n2"; then
		iniSet "video_shared_context" "true"
		iniSet "reicast_threaded_rendering"  "enabled"
		else iniSet "video_shared_context" "true"
		fi
		
        addEmulator 1 "$md_id" "$system" "$md_inst/flycast_libretro.so </dev/null"
        addSystem "$system"
    done
    

    local def=0
    isPlatform "kms" && def=1
    # segfaults on the rpi without redirecting stdin from </dev/null
    addEmulator $def "$md_id" "dreamcast" "$md_inst/flycast_libretro.so </dev/null"
    addSystem "dreamcast"			 
}
