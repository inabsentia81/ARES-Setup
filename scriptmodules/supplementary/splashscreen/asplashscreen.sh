#!/bin/sh

ROOTDIR=""
DATADIR=""
REGEX_VIDEO=""
REGEX_IMAGE=""

# Load user settings
. /opt/ares/configs/all/splashscreen.cfg

is_fkms() {
    if grep -q okay /proc/device-tree/soc/v3d@7ec00000/status 2> /dev/null || grep -q okay /proc/device-tree/soc/firmwarekms@7e600000/status 2> /dev/null ; then
        return 0
    else
        return 1
    fi
}

do_start () {
    local config="/etc/splashscreen.list"
    local line
    local re="$REGEX_VIDEO\|$REGEX_IMAGE"
	local omxiv="/opt/ares/supplementary/omxiv/omxiv"
    case "$RANDOMIZE" in
        disabled)
            line="$(head -1 "$config")"
            ;;
        ares)
            line="$(find "$ROOTDIR/supplementary/splashscreen" -type f | grep "$re" | shuf -n1)"
            ;;
        custom)
            line="$(find "$DATADIR/splashscreens" -type f | grep "$re" | shuf -n1)"
            ;;
        all)
            line="$(find "$ROOTDIR/supplementary/splashscreen" "$DATADIR/splashscreens" -type f | grep "$re" | shuf -n1)"
            ;;
        list)
            line="$(cat "$config" | shuf -n1)"
            ;;
    esac
    if $(echo "$line" | grep -q "$REGEX_VIDEO"); then
        # wait for dbus
        while ! pgrep "dbus" >/dev/null; do
            sleep 1
        done

        if grep -q "ODROID-N2" /sys/firmware/devicetree/base/model 2>/dev/null; then
            mpv -vo sdl -fs "$line"
        elif grep -q "RockPro64" /sys/firmware/devicetree/base/model 2>/dev/null; then
            mpv -vo sdl -fs "$line"    
        elif grep -q "Odroid XU4" /sys/firmware/devicetree/base/model 2>/dev/null; then
		ffplay -x 1920 -y 1080 -autoexit "$line"
		#elif grep -q "NVIDIA Jetson Nano Developer Kit" /sys/firmware/devicetree/base/model 2>/dev/null; then
		#TODO
		else omxplayer --no-osd -o both -b --layer 10000 "$line"    
        fi
    elif $(echo "$line" | grep -q "$REGEX_IMAGE"); then
        if [ "$RANDOMIZE" = "disabled" ]; then
            local count=$(wc -l <"$config")
        else
            local count=1
        fi
        [ $count -eq 0 ] && count=1
        [ $count -gt 12 ] && count=12

        # Default duration is 12 seconds, check if configured otherwise
        [ -z "$DURATION" ] && DURATION=12
        local delay=$((DURATION/count))
        if [ "$RANDOMIZE" = "disabled" ]; then
            fbi -T 2 -once -t $delay -noverbose -a -l "$config" >/dev/null 2>&1
        else
            fbi -T 2 -once -t $delay -noverbose -a "$line" >/dev/null 2>&1
        fi
    fi
    exit 0
}

case "$1" in
    start|"")
        do_start &
        ;;
    restart|reload|force-reload)
        echo "Error: argument '$1' not supported" >&2
        exit 3
       ;;
    stop)
        # No-op
        ;;
    status)
        exit 0
        ;;
    *)
        echo "Usage: asplashscreen [start|stop]" >&2
        exit 3
        ;;
esac
